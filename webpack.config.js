require('dotenv').config();

const os = require('os');
const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';

const projectDir = __dirname;
const srcDir = path.resolve(projectDir, 'src');
const outDir = path.resolve(projectDir, 'dist');

const host = os.platform() === 'linux' ? '0.0.0.0' : '127.0.0.1';
const port = 3000;

const jsEntries = {
  app: path.resolve(srcDir, 'index.js')
};

const env = {
  NODE_ENV: process.env.NODE_ENV
};

Object.keys(env).forEach((key) => {
  env[key] = JSON.stringify(env[key]);
});

const commonPlugins = [
  new webpack.DefinePlugin({
    'process.env': env
  }),

  new HtmlWebpackPlugin({
    filename: 'index.html',
    template: path.resolve(srcDir, 'index.html')
  }),
];

const productionPlugins = [
  new CleanWebpackPlugin(),

  new MiniCssExtractPlugin({
    filename: '[name]-[chunkhash].css'
  })
];

const miniCssExtractPluginLoaderCfg = {
  loader: MiniCssExtractPlugin.loader
};

const styleLoaderCfg = {
  loader: 'style-loader'
};

const cssLoaderCfg = {
  loader: 'css-loader',
  options: {
    sourceMap: !isProduction,
    importLoaders: 3,
    modules: {
      mode: 'global',
      localIdentName: isProduction ? '[hash:base64]' : '[path][name]__[local]'
    }
  }
};

const resolveUrlLoaderCfg = {
  loader: 'resolve-url-loader',
  options: {
    sourceMap: !isProduction
  }
};

const postCssLoaderCfg = {
  loader: 'postcss-loader'
};

const sassLoaderCfg = {
  loader: 'sass-loader',
  options: {
    sourceMap: true // it's required for resolve-url-loader
  }
};

const assetNamePattern = 'assets/[name]_[hash].[ext]';

const assetsLoaders = [
  {
    loader: 'url-loader',
    options: {
      limit: 3000,
      name: assetNamePattern
    }
  }
];

if (isProduction) {
  assetsLoaders.push({
    loader: 'image-webpack-loader'
  });
}

module.exports = {
  context: projectDir,
  target: 'web',
  mode: isProduction ? 'production' : 'development',
  entry: jsEntries,
  output: {
    path: outDir,
    publicPath: '/',
    filename: isProduction ? '[name]-[chunkhash].js' : '[name].js',
    chunkFilename: isProduction ? '[name]-[chunkhash].chunk.js' : '[name].chunk.js'
  },
  devtool: isProduction ? false : 'source-map',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: [
          /node_modules/
        ],
        use: [
          'babel-loader'
        ]
      },
      {
        test: /\.css$/,
        use: [
          isProduction ? miniCssExtractPluginLoaderCfg : styleLoaderCfg,
          cssLoaderCfg,
          postCssLoaderCfg,
          resolveUrlLoaderCfg
        ]
      },
      {
        test: /\.(scss|sass)$/,
        use: [
          isProduction ? miniCssExtractPluginLoaderCfg : styleLoaderCfg,
          cssLoaderCfg,
          postCssLoaderCfg,
          resolveUrlLoaderCfg,
          sassLoaderCfg
        ]
      },
      {
        test: /\.(gif|png|jpe?g|ico|svg)$/,
        use: assetsLoaders
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: assetNamePattern
            }
          }
        ]
      }
    ]
  },
  plugins: [
    ...commonPlugins,
    ...(isProduction ? productionPlugins : [])
  ],
  resolve: {
    extensions: ['.js']
  },
  performance: {
    hints: false
  },
  devServer: {
    host,
    port,
    historyApiFallback: true,
    stats: 'minimal',
    contentBase: srcDir,
    disableHostCheck: true
  }
};
