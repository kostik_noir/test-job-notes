import React from 'react';
import { connect } from 'react-redux';
import * as notes from '../../state/notes';
import styles from './styles.scss';

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(notes.actionCreators.create());
  }
});

export default connect(null, mapDispatchToProps)(UiStateWithoutNotes);

function UiStateWithoutNotes({ onClick }) {
  return (
    <div className={ styles.root }>
      <div className={ styles.btn } onClick={ onClick }>
        <i className={ styles.btnIcon }></i>
        <div>Create a note</div>
      </div>
    </div>
  );
}
