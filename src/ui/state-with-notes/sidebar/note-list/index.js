import React from 'react';
import { connect } from 'react-redux';
import * as notes from '../../../../state/notes';
import { create as createItem } from './item';
import styles from './styles.scss';

const mapStateToProps = state => ({
  noteIds: notes.selectors.getNotes(state).map(obj => obj.get('id')).toArray()
});

export default connect(mapStateToProps)(NoteList);

function NoteList({ noteIds }) {
  return (
    <div className={ styles.root }>
      {
        noteIds.map((id, index) => React.createElement(createItem({ id, index }), { key: id }))
      }
    </div>
  );
}
