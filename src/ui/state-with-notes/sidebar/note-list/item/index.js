import React from 'react';
import { connect } from 'react-redux';
import { Confirm, Prompt } from 'react-st-modal';
import * as notes from '../../../../../state/notes';
import Submenu from './submenu';
import styles from './styles.scss';

export const create = ({ id, index }) => {
  const mapStateToProps = state => ({
    title: notes.selectors.getNoteById(state, id).get('title'),
    isSelected: notes.selectors.isSelectedNote(state, id),
    id,
    index
  });

  const mapDispatchToProps = dispatch => ({
    onRenameConfirmed: (title) => {
      dispatch(notes.actionCreators.rename(id, title));
    },
    onItemPressed: () => {
      dispatch(notes.actionCreators.selected(id));
    },
    onDeleteConfirmed: () => {
      dispatch(notes.actionCreators.remove(id));
    }
  });

  return connect(mapStateToProps, mapDispatchToProps)(Item);
};

function Item({ index, id, title, isSelected, onRenameConfirmed, onItemPressed, onDeleteConfirmed }) {
  const onDeleteBtnPressed = async () => {
    const isConfirmed = await Confirm(`Delete a note "${title}"?`, 'Delete a note');
    if (!isConfirmed) {
      return;
    }
    onDeleteConfirmed();
  };

  const onRenameBtnPressed = async () => {
    const result = await Prompt('Rename a note', {
      defaultValue: title,
      isRequired: true,
      errorText: 'Note title cannot be empty'
    });

    if (typeof result === 'undefined') {
      return;
    }

    onRenameConfirmed(result);
  };

  let classNames = [styles.root];
  if (isSelected) {
    classNames.push(styles.isActive);
  }
  classNames = classNames.join(' ');

  return (
    <div className={ classNames } style={ { zIndex: -index } } onClick={ onItemPressed }>
      <div className={ styles.title }>{ title }</div>
      <div className={ styles.icon }>
        <i className="fa fa-ellipsis-v"></i>
        <div className={ styles.submenu }>
          <Submenu onDeleteBtnPressed={ onDeleteBtnPressed } onRenameBtnPressed={ onRenameBtnPressed }/>
        </div>
      </div>
    </div>
  );
}
