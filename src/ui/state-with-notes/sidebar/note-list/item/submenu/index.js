import React from 'react';
import styles from './styles.scss';

export default ({ onRenameBtnPressed, onDeleteBtnPressed }) => {
  const onRenameBtn = event => {
    event.preventDefault();
    event.stopPropagation();
    onRenameBtnPressed();
  };

  const onDeleteBtn = event => {
    event.preventDefault();
    event.stopPropagation();
    onDeleteBtnPressed();
  };

  return (
    <div className={ styles.root }>
      <div className={ styles.list }>
        <div className={ styles.btn } onClick={ onRenameBtn }>
          <i className="fa fa-edit"></i>
          <div>rename</div>
        </div>
        <div className={ styles.btn } onClick={ onDeleteBtn }>
          <i className="fa fa-trash"></i>
          <div>delete</div>
        </div>
      </div>
    </div>
  );
}
