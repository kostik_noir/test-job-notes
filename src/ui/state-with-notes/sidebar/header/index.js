import React from 'react';
import { connect } from 'react-redux';
import * as notes from '../../../../state/notes';
import styles from './styles.scss';

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(notes.actionCreators.create());
  }
});

export default connect(null, mapDispatchToProps)(Header);

function Header({ onClick }) {
  return (
    <div className={ styles.root }>
      <div className={ styles.addBtn } onClick={ onClick }>
        <i className={ styles.addBtnIcon }></i>
        <div>Create a new note</div>
      </div>
    </div>
  );
}
