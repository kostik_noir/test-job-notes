import React from 'react';
import Header from './header';
import NoteList from './note-list';
import styles from './styles.scss';

export default () => (
  <div className={ styles.root }>
    <Header/>
    <div className={ styles.noteList }>
      <NoteList/>
    </div>
  </div>
);
