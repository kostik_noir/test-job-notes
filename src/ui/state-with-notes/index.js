import React from 'react';
import Sidebar from './sidebar';
import ContentEditor from './content-editor';
import styles from './styles.scss';

export default () => (
  <div className={ styles.root }>
    <div className={ styles.sidebar }>
      <Sidebar/>
    </div>
    <div className={ styles.contentEditor }>
      <ContentEditor/>
    </div>
  </div>
);
