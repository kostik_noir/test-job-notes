import React from 'react';
import { connect } from 'react-redux';
import * as notes from '../../../state/notes';
import styles from './styles.scss';

const mapStateToProps = state => {
  const selectedNoteId = notes.selectors.getSelectedNoteId(state);
  const note = notes.selectors.getNoteById(state, selectedNoteId);

  return {
    noteId: selectedNoteId,
    content: note.get('content')
  };
};

const mapDispatchToProps = dispatch => ({
  onContentChanged: (noteId, content) => {
    dispatch(notes.actionCreators.contentChanged(noteId, content));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ContentEditor);

function ContentEditor({ noteId, content, onContentChanged }) {
  const onChange = (event) => {
    onContentChanged(noteId, event.target.value);
  };

  return (
    <div className={ styles.root }>
      <textarea
        className={ styles.textArea }
        placeholder="Write something here..."
        value={ content }
        onChange={ onChange }
      ></textarea>
    </div>
  );
}
