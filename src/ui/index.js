import React from 'react';
import { connect } from 'react-redux';
import { selectors } from '../state/notes'
import StateWithoutNotes from './state-without-notes';
import StateWithNotes from './state-with-notes';
import styles from './styles.scss';

const mapStateToProps = state => ({
  hasNotes: selectors.hasNotes(state)
});

export default connect(mapStateToProps, null)(App);

function App({ hasNotes }) {
  return (
    <div className={ styles.root }>
      { hasNotes ? <StateWithNotes/> : <StateWithoutNotes/> }
    </div>
  );
}
