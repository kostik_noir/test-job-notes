import { createStore, applyMiddleware } from 'redux';
import { combineReducers } from 'redux-immutable';
import * as notes from './notes';

const reducers = {
  [notes.mountPoint]: notes.reducer
};

const middleware = [
  ...notes.middleware
];

export default createStore(combineReducers(reducers), applyMiddleware(...middleware));
