import { createSelector } from 'reselect';
import { mountPoint } from './constants';

export const getNotes = state => state.getIn([mountPoint, 'notes']);

export const getSelectedNoteId = state => state.getIn([mountPoint, 'selectedNoteId']);

export const hasNotes = createSelector(getNotes, notes => notes.size > 0);

export const getNoteById = (state, id) => getNotes(state).find(item => item.get('id') === id);

export const isSelectedNote = (state, id) => id === getSelectedNoteId(state);
