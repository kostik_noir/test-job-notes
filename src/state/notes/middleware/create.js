import * as constants from '../constants';
import * as actionCreators from '../action-creators';

const newNoteTitle = 'Untitled';

export default store => next => action => {
  next(action);

  switch (action.type) {
    case constants.create:
      onCreateNoteRequested();
      break;
  }

  function onCreateNoteRequested() {
    store.dispatch(actionCreators.created(generateNewNoteId(), newNoteTitle));
  }
}

function generateNewNoteId() {
  return Date.now();
}
