import { Map, fromJS } from 'immutable';
import * as constants from './constants';

const initialState = fromJS({
  notes: [],
  selectedNoteId: null
});

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case constants.created:
      return onNoteCreated(state, action);
    case constants.removed:
      return onNoteRemoved(state, action);
    case constants.renamed:
      return onNoteRenamed(state, action);
    case constants.contentChanged:
      return onContentChanged(state, action);
    case constants.selected:
      return onNoteSelected(state, action);
    default:
      return state;
  }
};

function onNoteCreated(state, action) {
  const { payload: { id, title } } = action;
  const newNote = Map({ id, title, content: '' });
  return state
    .update('notes', notes => notes.push(newNote))
    .set('selectedNoteId', id);
}

function onNoteRemoved(state, action) {
  const { payload: { id } } = action;
  const notes = state.get('notes');
  const index = notes.findIndex(obj => obj.get('id') === id);
  if (index === -1) {
    return state;
  }

  if (id !== state.get('selectedNoteId')) {
    return state.update('notes', notes => notes.delete(index));
  }

  let selectedNoteId;

  if (notes.size === 1) {
    selectedNoteId = null;
  } else {
    selectedNoteId = notes.get(index - 1).get('id');
  }

  return state
    .update('notes', notes => notes.delete(index))
    .set('selectedNoteId', selectedNoteId);
}

function onNoteRenamed(state, action) {
  const { payload: { id, title } } = action;
  const index = state
    .get('notes')
    .findIndex(obj => obj.get('id') === id);

  if (index === -1) {
    return state;
  }

  return state.setIn(['notes', index, 'title'], title);
}

function onContentChanged(state, action) {
  const { payload: { id, content } } = action;
  const index = state
    .get('notes')
    .findIndex(obj => obj.get('id') === id);

  if (index === -1) {
    return state;
  }

  return state.setIn(['notes', index, 'content'], content);
}

function onNoteSelected(state, action) {
  const { payload: { id } } = action;
  return state.set('selectedNoteId', id)
}
