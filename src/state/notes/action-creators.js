import * as constants from './constants';

export const create = () => ({
  type: constants.create
});

export const created = (id, title) => ({
  type: constants.created,
  payload: { id, title }
});

export const remove = (id) => ({
  type: constants.removed,
  payload: { id }
});

export const rename = (id, title) => ({
  type: constants.renamed,
  payload: { id, title }
});

export const contentChanged = (id, content) => ({
  type: constants.contentChanged,
  payload: { id, content }
});

export const selected = (id) => ({
  type: constants.selected,
  payload: { id }
});
