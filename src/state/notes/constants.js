export const mountPoint = 'notes';
export const create = `${mountPoint}/create`;
export const created = `${mountPoint}/created`;
export const removed = `${mountPoint}/removed`;
export const renamed = `${mountPoint}/renamed`;
export const contentChanged = `${mountPoint}/contentChanged`;
export const selected = `${mountPoint}/selected`;
