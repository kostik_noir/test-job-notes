import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './ui';
import store from './state';

const app = (
  <Provider store={ store }>
    <App/>
  </Provider>
);

render(app, document.querySelector('[data-app]'));
